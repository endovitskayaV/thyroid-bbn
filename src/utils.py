from typing import Any

import pandas
from pandas import DataFrame
from pandas._typing import FilePathOrBuffer

from settings import SEP


def to_csv(df: DataFrame, path: FilePathOrBuffer) -> None:
    df.to_csv(path, sep=SEP, index=False)


def read_csv(path: FilePathOrBuffer, sep=SEP) -> DataFrame:
    return pandas.read_csv(path, sep=sep)


def write_to_file(path: FilePathOrBuffer, content: Any) -> None:
    with open(path, 'w') as file:
        file.write(str(content))
