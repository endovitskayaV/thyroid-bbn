import os
import sys
from collections import Counter

import bnlearn as bn
import pandas
from imblearn.over_sampling import RandomOverSampler
from matplotlib import pyplot as plt
from pandas import DataFrame
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report, roc_curve, \
    auc

from settings import RAW_CSV_PATH, TSH, LABEL, NEGATIVE, HIGH, LOW, SEX, FEMALE, MALE, \
    TEST_CSV_PATH, IS_PREGNANT, T3, TT4, T4U, FTI, REPORTS_PATH, POSITIVE, AGE
from src.utils import read_csv, write_to_file, to_csv

PROBABILITY = 'p'

INPUT_COLUMNS = [TSH, SEX, IS_PREGNANT, T3, TT4, T4U, FTI, AGE, ]


def exec_example():
    set_terminal_size_envs()
    df = read_csv(RAW_CSV_PATH, sep=',')
    df = clean_df(df)
    df = oversample(df)

    edges = [
        (TSH, LABEL),
        (SEX, LABEL),
        (IS_PREGNANT, LABEL),
        (SEX, IS_PREGNANT),
        (T3, LABEL),
        (TT4, LABEL),
        (T4U, LABEL),
        (FTI, LABEL),
        (AGE, LABEL),
    ]
    DAG = bn.make_DAG(edges)
    bn.plot(DAG)

    model = bn.parameter_learning.fit(DAG, df, methodtype='maximumlikelihood')

    # update_label_probs(model)
    # check_bbn(model)

    test_df = clean_df(read_csv(TEST_CSV_PATH, ','))
    probs_df = bn.predict(model, test_df, variables=[LABEL])
    to_csv(probs_df, REPORTS_PATH / "pred.csv")

    y_proba = calculate_proba_df(probs_df)
    y_pred = probs_df[LABEL].apply(lambda value: NEGATIVE if value == 0 else POSITIVE)
    y_true = test_df[LABEL].apply(lambda value: NEGATIVE if value == 0 else POSITIVE)
    classes = [POSITIVE, NEGATIVE]

    write_to_file(str(REPORTS_PATH / 'classification_report.txt'), classification_report(y_true, y_pred))
    confusion_matrix_(classes, y_pred, y_true)
    multiclass_roc_auc(classes, y_proba, y_true, str(REPORTS_PATH / "roc_auc.png"))


def confusion_matrix_(classes, y_pred, y_true):
    cm = confusion_matrix(y_true, y_pred, labels=classes)
    plt.clf()
    fig, ax = plt.subplots(figsize=(12, 8))
    cmp = ConfusionMatrixDisplay(cm, display_labels=classes)
    cmp.plot(ax=ax)
    ax.figure.savefig(str(REPORTS_PATH / 'confusion_matrix.png'))


def calculate_proba_df(probs_df) -> DataFrame:
    y_proba = probs_df.copy()
    y_proba[POSITIVE] = y_proba.apply(lambda row: row[PROBABILITY] if row[LABEL] == 1 else 1 - row[PROBABILITY], axis=1)
    y_proba[NEGATIVE] = y_proba[POSITIVE].apply(lambda p: 1 - p)
    y_proba = y_proba[[POSITIVE, NEGATIVE]]
    return y_proba


def set_terminal_size_envs() -> None:
    os.environ["COLUMNS"] = str(sys.maxsize)
    os.environ["LIMES"] = "24"


def multiclass_roc_auc(classes, probs, y_test, output_filepath: str) -> None:
    plt.clf()
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    y_true: DataFrame = pandas.get_dummies(y_test, columns=classes)
    for i, clazz in enumerate(classes):
        fpr[i], tpr[i], thresholds = roc_curve(y_true.loc[:, clazz], probs.loc[:, clazz])
        roc_auc[i] = auc(fpr[i], tpr[i])
        plt.plot(fpr[i], tpr[i], label=classes[i] + ' (area = %s)' % roc_auc[i])
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC curve')
        plt.legend(loc="lower right")
        plt.savefig(output_filepath)


def clean_df(df) -> DataFrame:
    df = df[INPUT_COLUMNS + [LABEL]]
    encode_categorials(df)
    return df


def check_bbn(model):
    print('TSH high')
    bn.inference.fit(model, variables=[LABEL], evidence={TSH: HIGH})
    print('TSH low')
    bn.inference.fit(model, variables=[LABEL], evidence={TSH: LOW})
    print(MALE)
    bn.inference.fit(model, variables=[LABEL], evidence={SEX: MALE})
    print(FEMALE)
    bn.inference.fit(model, variables=[LABEL], evidence={SEX: FEMALE})


def update_label_probs(model):
    cpd = [cpd for cpd in model['model'].cpds if cpd.variable == LABEL][0]
    # correct probs for non-pregnant females with high TSH
    cpd.values[0][0][0][0], cpd.values[1][0][0][0] = cpd.values[1][0][0][0], cpd.values[0][0][0][0]
    print("New CPD of {}".format(LABEL))
    print(cpd)


def oversample(df) -> DataFrame:
    X = df[INPUT_COLUMNS]
    y = df[LABEL]

    max_amount: int = Counter(y).most_common()[0][1]
    over_sampler: RandomOverSampler = RandomOverSampler({0: max_amount, 1: max_amount // 3})
    X, y = over_sampler.fit_resample(X, y)
    return pandas.concat([X, y], axis=1)


def encode_categorials(df):
    df[LABEL] = df[LABEL].apply(lambda value: 0 if value == NEGATIVE else 1)
    df[SEX] = df[SEX].apply(lambda value: MALE if value == 1 else FEMALE)

    numerical_columns = [TSH, T3, TT4, T4U, FTI, AGE]
    for column in numerical_columns:
        if column in df.columns:
            threshold = df[column].mean()
            print("{}: {}".format(column, threshold))
            df[column] = df[column].apply(lambda value: HIGH if value > threshold else LOW)


if __name__ == '__main__':
    exec_example()
