import pathlib

# ------------------ CONSTANTS ------------------#

SEP = ';'

TSH = 'TSH'
LABEL = 'label'
SEX = 'sex'
IS_PREGNANT = 'pregnant'
T3 = 'T3'
TT4 = 'TT4'
T4U = 'T4U'
FTI = 'FTI'
AGE = 'age'
NEGATIVE = 'negative'
POSITIVE = 'positive'
HIGH = 'High'
LOW = 'Low'
MALE = 'male'
FEMALE = 'female'

# ------------------ PATHS ------------------#

DATA_PATH = pathlib.Path(__file__).parent / "data"
REPORTS_PATH = pathlib.Path(__file__).parent / "reports"

RAW_CSV_PATH = DATA_PATH / "data.csv"
TEST_CSV_PATH = DATA_PATH / "test.csv"
